import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';

dayjs.extend(relativeTime);

const BUGS_URL = 'https://bugs.kde.org';

function isBugzillaLink(link) {
  // E.g. https://bugs.kde.org/402687
  return link.href.indexOf('bugs.kde.org') > -1;
}

/**
 * @typedef {Object} ParsedBugLink
 * @property {string} href
 * @property {string} domain
 * @property {number} bugId
 */

/**
  *
  * @param {HTMLAnchorElement} link
  * @returns {ParsedBugLink}
  */
function parseBugLink(link) {
  const match = link.href.match(/(^.+:\/\/bugs.kde.org\/)(\d+)$/);
  if (match) {
    return {
      href: link.href,
      domain: match[1],
      bugId: Number(match[2]),
    };
  }
  return {
    href: link.href,
    domain: null,
    bugId: null,
  };
}

/** @typedef {string} DateTime */

/**
 * @typedef {Object} BugComment
 * @property {number} id
 * @property {number} bug_id
 * @property {number} attachment_id
 * @property {number} count
 * @property {string} text
 * @property {string} creator
 * @property {DateTime} time
 * @property {DateTime} creation_time
 * @property {boolean} is_private
 * @property {boolean} is_markdown
 * @property {string[]} tags
 */

/**
 * @typedef {Object} BugzillaApiCommentResponse
 * @property {Object.<string, {comments: BugComment[]}>} bugs
 * @property {any[]} comments
 */

/**
 * @typedef {Object} BugAuthorDetails
 * @property {string} email
 * @property {number} id
 * @property {string} name
 * @property {string} real_name
 */

/**
 * @typedef {Object} BugData
 * @property {string} summary
 * @property {string} status
 * @property {string} resolution
 * @property {string} product
 * @property {string} component
 * @property {DateTime} creation_time
 * @property {BugAuthorDetails} creator_detail
 */

/**
 * @typedef {Object} BugzillaApiBugResponse
 * @property {BugData[]} bugs
 * @property {any[]} faults
 */

/**
 * @typedef {Object} BugAttachment
 * @property {string} data Base64 data
 * @property {number} size
 * @property {DateTime} creation_time
 * @property {DateTime} last_change_time
 * @property {number} id
 * @property {number} bug_id
 * @property {string} file_name
 * @property {string} summary
 * @property {string} content_type
 * @property {boolean} is_private
 * @property {boolean} is_obsolete
 * @property {boolean} is_patch
 * @property {string} creator
 * @property {string[]} flags
 */

class BugzillaApi {
  constructor(url, apiExtension = '/rest') {
    this.baseUrl = url;
    this.apiExtension = apiExtension;
    this.url = `${this.baseUrl}${this.apiExtension}`;
    /** @type {Object.<string, any>} */
    this.cache = {};
  }

  /**
   *
   * @param {string} route
   * @param {(data) => any} processor
   */
  async request(route, processor = null) {
    const requestUrl = this.url + route;
    if (!(requestUrl in this.cache)) {
      const fetchResponse = await fetch(requestUrl);
      let response = await fetchResponse.json();
      if (typeof processor === 'function') {
        response = processor(response);
      }
      this.cache[requestUrl] = response;
    }
    return this.cache[requestUrl];
  }

  /**
   * @param {number} bugId
   * @returns {Promise<BugData>}
   */
  getBug(bugId, bugProperties = ['summary', 'status', 'resolution', 'product', 'component', 'creation_time', 'creator']) {
    return this.request(`/bug/${bugId}?include_fields=${bugProperties.join(',')}`, (response) => {
      console.log(response);
      let data = null;
      if ('bugs' in response && Array.isArray(response.bugs) && response.bugs.length === 1) {
        data = response.bugs[0];
      }
      return data;
    });
  }

  /**
   * @param {number} bugId
   * @returns {Promise<BugComment[]>}
   */
  getComments(bugId) {
    return this.request(`/bug/${bugId}/comment`, (response) => {
      let comments = null;
      if ('bugs' in response && bugId in response.bugs) {
        ({ comments } = response.bugs[bugId]);
      }
      return comments;
    });
  }

  /**
   *
   * @param {number} attachmentId
   * @returns {Promise<BugAttachment>}
   */
  getAttachment(attachmentId) {
    return this.request(`/bug/attachment/${attachmentId}`, (response) => {
      let attachment = null;
      if ('attachments' in response && attachmentId in response.attachments) {
        attachment = response.attachments[attachmentId];
      }
      return attachment;
    });
  }
}

const bz = new BugzillaApi(BUGS_URL);

/**
 *
 * @param {string} comment
 */
function getCommentSummary(comment) {
  const summaryIndex = comment.search(/SUMMARY\n/);
  let summary = null;
  if (summaryIndex > -1) {
    summary = comment.slice(summaryIndex, comment.length);
    summary = summary.replace(/SUMMARY\n/, '');
  }
  const headings = ['STEPS TO REPRODUCE', 'OBSERVED RESULT', 'EXPECTED RESULT', 'SOFTWARE/OS VERSIONS'];
  for (const heading of headings) {
    const headingIndex = summary.indexOf(heading);
    if (headingIndex > -1) {
      summary = summary.slice(0, headingIndex);
    }
  }
  return summary;
}

/**
 *
 * @param {HTMLAnchorElement} link
 */
function addBugLinkPopup(link) {
  const popup = document.createElement('div');
  popup.classList.add(...['bug-popup', 'loading']);
  const loader = document.createElement('div');
  loader.classList.add('loader');
  popup.appendChild(loader);
  link.appendChild(popup);
  return popup;
}

/**
 *
 * @param {HTMLElement} popup
 * @param {string} value
 */
function addBugDataToPopup(popup, value) {
  popup.innerText += value;
  popup.innerHTML += '<br>';
}

const fetchingData = {};

/**
 *
 * @param {HTMLAnchorElement} link
 */
function addHoverListener(link) {
  link.addEventListener('mouseover', async () => {
    const { bugId } = parseBugLink(link);
    const popupAlreadyAdded = link.querySelector('.bug-popup');
    if (!popupAlreadyAdded && (!(link.href in fetchingData) || !fetchingData[link.href])) {
      fetchingData[link.href] = true;
      try {
        const popup = addBugLinkPopup(link);

        const header = document.createElement('h1');
        popup.appendChild(header);

        const bugSummaryEl = document.createElement('div');
        bugSummaryEl.classList.add('summary');
        popup.appendChild(bugSummaryEl);

        const bugCommentEl = document.createElement('div');
        bugCommentEl.classList.add('comment');
        popup.appendChild(bugCommentEl);

        const info = await bz.getBug(bugId);
        popup.classList.remove('loading');
        header.innerText = info.summary;
        const creationTime = dayjs(info.creation_time).fromNow();
        const creator = `<a href='mailto:${info.creator_detail.email}'>${info.creator_detail.real_name}</a>`;
        bugSummaryEl.innerHTML += `<div>Opened ${creationTime} by ${creator}</div>`;
        if (info.component.toLowerCase() !== info.product.toLowerCase()) {
          bugSummaryEl.innerHTML += `<div class='path'><span>${info.product}</span>: <span>${info.component}</span></div>`;
        } else {
          bugSummaryEl.innerHTML += `<div class='path'><span>${info.product}</span></div>`;
        }

        const comments = await bz.getComments(bugId);
        if (comments.length > 0) {
          const mainComment = comments[0];
          const commentEl = document.createElement('p');
          let commentText = mainComment.text;

          // Start at summary heading if it exists
          const summary = getCommentSummary(commentText);
          if (summary) {
            commentText = summary;
          }

          commentText = commentText.trim();

          // Wrap if too long
          const wrapLength = 120;
          if (commentText.length > wrapLength) {
            commentText = `${commentText.slice(0, wrapLength)}...`;
          }

          commentEl.innerText = commentText;
          bugCommentEl.appendChild(commentEl);

          if (mainComment.attachment_id) {
            const attachment = await bz.getAttachment(mainComment.attachment_id);
            if (attachment.content_type.indexOf('image') > -1) {
              const attachmentEl = document.createElement('img');
              attachmentEl.classList.add('attachment');
              attachmentEl.src = `data:${attachment.content_type};base64,${attachment.data}`;
              attachmentEl.alt = attachment.summary;
              attachmentEl.style.maxWidth = '400px';
              popup.appendChild(attachmentEl);
            }
          }
        }
      } catch (error) {
        throw error;
      } finally {
        fetchingData[link.href] = false;
      }
    }
  });
}

async function main() {
  const links = document.querySelectorAll('a');

  const bugzillaLinks = Array.from(links).filter(isBugzillaLink);
  bugzillaLinks.map(link => link.classList.add('bugzilla-link'));
  bugzillaLinks.map(link => addHoverListener(link));
}
main();
