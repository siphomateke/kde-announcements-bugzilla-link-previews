import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import builtins from 'rollup-plugin-node-builtins';
import copy from 'rollup-plugin-copy';

export default {
  input: 'src/main.js',
  output: {
    file: 'dist/main.js',
    format: 'iife',
    sourcemap: true,
  },
  plugins: [
    nodeResolve(),
    commonjs(),
    builtins(),
    copy({
      targets: [
        'src/manifest.json',
        'src/css',
      ],
      outputFolder: 'dist',
    }),
  ],
};
