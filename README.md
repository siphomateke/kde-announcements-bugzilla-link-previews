# KDE Announcements Bugzilla Link Previews

Adds previews to KDE Bugzilla links (https://bugs.kde.org/) on the [KDE Announcements website](https://kde.org/announcements/).
